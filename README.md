**Tickets API** is a REST service to sort travel tickets. 
It wait for **POST** request from client with parameters to be filled.
As it is default Flask application you most probably will find it on port 5000.
Example of sending request using Python:
 


*  import requests


*  URL = "http://127.0.0.1:5000/"


*  PARAMS={'start':'Madrid','dest':'New York JFK','cards':[{'type':'plane',
                  'start':'Girona Airport',
                  'dest':'Stockholm',
                  'number':'SK455',
                  'gate':'45B',
                  'seat':'3A',
                  'baggage':'344'
                  },
                 {'type':'train',
                  'start':'Madrid',
                  'dest':'Barcelona',
                  'number':'78A',
                  'seat':'45B'
                  },
                 {'type': 'bus',
                  'name': 'airport',
                  'start': 'Barcelona',
                  'dest': 'Girona Airport',
                  'seat': ''
                  },
                {'type':'plane',
                  'start':'Stockholm',
                  'dest':'New York JFK',
                  'number':'SK22',
                  'gate':'22',
                  'seat':'7B',
                 'isTransfer':'true'
                 }
                 ]}

*  r = requests.post(url = URL,json =PARAMS)

*  if r.ok:
    print (r.json())


Which in turn returns:

Take train 78A from Madrid to Barcelona. Sit in seat 45B.

Take airport bus from Barcelona to Girona Airport. No seat assignment.

From Girona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A.
Baggage drop at ticket counter 344.

From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B.
Baggage will be automatically transferred from your last leg.

You have arrived at your destination.

  **Note:** while requesting API you should include all field for given type of 
ticket if you do not want to consider them you should set it empty like: {'seat':''}


