def get_path(cards, start, end, result):
    if start == end:
        return True
    tickets_filtered = []
    for card in cards:
        if card['start'] == start and card['dest'] not in [y['start'] for y in result]:
            tickets_filtered.append(card)
    if len(tickets_filtered) == 0:
        result.pop()
    else:
        for ticket in tickets_filtered:
            result.append(ticket)
            start = ticket['dest']
            if get_path(cards, start, end, result=result):
                return True
        result.clear()