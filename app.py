import sys
import traceback

from flask import Flask, request
from flask import jsonify
from getPath import get_path
from Ticket import *

app = Flask(__name__)


@app.route('/', methods=['POST'])
def request_handler():
    try:
        params = request.json
        path = []
        get_path(params['cards'], params['start'],
                 params['dest'], path)
        result = ''
        for point in path:
            if point['type'] == 'plane':
                result += (str(PlaneTicket(start=point['start'], dest=point['dest'], number=point['number'],
                                           gate=point['gate'], seat=point['seat'],
                                           baggage=point['baggage'] if 'baggage' in point else None,
                                           isTransfer=False if 'baggage' in point else True)) + '\n')
            elif point['type'] == 'bus':
                result += (str(
                    BusTicket(start=point['start'], dest=point['dest'],
                              name=point['name'],
                              seat=point['seat'] if point['seat'] in point else None)) + '\n')
            elif point['type'] == 'train':
                result += (str(TrainTicket(start=point['start'], dest=point['dest'], number=point['number'],
                                           seat=point['seat'])) + '\n')
        result += 'You have arrived at your destination.'
        return jsonify(result)
    except:
        traceback.print_exc(file=sys.stdout)
        return jsonify(['Bad Request'])


if __name__ == '__main__':
    app.run()
