class Ticket:
    __slots__ = ['start', 'dest']

    def __init__(self, start, dest):
        self.start = start
        self.dest = dest


class TrainTicket(Ticket):
    __slots__ = ['number', 'seat']

    def __init__(self, start, dest, number, seat):
        super().__init__(start, dest)
        self.number = number
        self.seat = seat

    def __str__(self):
        return 'Take train %s from %s to %s. Sit in seat %s.' % (self.number, self.start, self.dest, self.seat)


class PlaneTicket(Ticket):
    __slots__ = ['number', 'gate', 'seat', 'baggage', 'isTransfer']

    def __init__(self, start, dest, number, gate, seat, baggage=None, isTransfer=False):
        super().__init__(start, dest)
        self.number = number
        self.seat = seat
        self.gate = gate
        self.baggage = baggage
        self.isTransfer = isTransfer

    def __str__(self):
        return 'From %s, take flight %s to %s. Gate %s, seat %s.' % (
            self.start, self.number, self.dest, self.gate, self.seat) + (
                   'Baggage drop at ticket counter %s.' % self.baggage if not self.isTransfer else
                   'Baggage will be '
                   'automatically transferred from your last leg.')


class BusTicket(Ticket):
    __slots__ = ['name', 'seat']

    def __init__(self, start, dest, name, seat=None):
        super().__init__(start, dest)
        self.name = name
        self.seat = seat

    def get_seat(self):
        return self.seat if self.seat else 'No seat assignment.'

    def __str__(self):
        return 'Take %s bus from %s to %s. %s' % (self.name, self.start, self.dest,
                                                  self.get_seat())
